This is MATE Tweak, a fork of [mintDesktop](https://github.com/linuxmint/mintdesktop).

  * MATE Tweak removes the Mint specific configuration options.

Personally I'm not the least bit interested in using the MATE Tweak but I 
see that it is regularly requested in the Ubuntu MATE community. So 
consider MATE Tweak a gift from me, to you :-)

Anyone interested in testing MATE Tweak I've made packages for Trusty and
Utopic, which you can install from the following PPA:

  * https://launchpad.net/~ubuntu-mate-dev/+archive/ubuntu/ppa
